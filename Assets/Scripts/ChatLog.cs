﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatLog : MonoBehaviour
{

    public Text chatLogText;


    public float chatLogHeight;
    public float chatLogWidth;

    float count = 2;

    public float chatLogInc;



    public void AddText(string battleText)
    {
        chatLogText.text = (chatLogText.text) + (battleText + "\n");


        Text tempText = chatLogText.GetComponent<Text>();
        int lines = tempText.cachedTextGenerator.lineCount;

        if (lines > 16)
        {
            RectTransform rt = chatLogText.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(chatLogWidth, chatLogHeight + (chatLogInc * count));
            count += 1;
        }

    }

    public void AddTextTest()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            AddText("10");
         

           
        }

    }

    private void Update()
    {
        AddTextTest();
    }





}
