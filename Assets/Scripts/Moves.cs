﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MoveRangeTypes { TARGETED, AOE, ALL}

public enum MoveEffects { ATTACKING, ATTACKINGPENETRATING, ATTACKINGMULTIPLE, ATTACKINGRECOIL, ATTACKINGDECREASING, ATTACKINGBOOSTING, ATTACKINGPRIORITY }

public enum Order { FIRST , SECOND, CLASH}
[CreateAssetMenu(menuName = "Move")]
public class Moves : ScriptableObject

{
    public string moveName;

    public string note = "Remmeber to capitalize stuff";

    public float moveDmg;

    public string moveDes;

    public Stats moveStat;

    public Types moveType;

    public MoveEffects moveEffect;

    public float movePow;

    public int moveRange;

    public MoveRangeTypes moveRangeType;

    public Status moveStatusType;

    public float moveRecoil;

    public float moveHeal;

    public Stats moveBoostStat;

    public float moveBoostAmt;

    public Stats moveDecreaseStat;

    public float moveDecreaseAmt;

    public float moveModifier;

    public float movePen;

    public int moveHits;

    public int moveUsage;

    public int moveUsageMax;


    

}
