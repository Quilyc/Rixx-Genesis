﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Types { FIRE, ICE, WATER, POISON, ESPER, ALIEN, AIR, MARTIAL, BEAST, NATURAL, EARTH, METAL, ELECTRIC, NONE }

public enum Stats { ATK, SPA, SPE, DEF, POW, SPD, ATKSPA, HP, ATKSPE, SPASPE, NONE }

public enum Status { BURNING, NONE}

public class Unit : MonoBehaviour
{
    public string unitName;

    public float unitSpa;

    public float unitSpe;

    public float unitPow;

    public float unitDef;

    public float unitSpd;

    public float unitAtk;

    public float unitMaxHP;

    public float unitCurHP;

    public float unitCurShield;

    public float unitMaxShield;

    public Types unitType;

    public string unitAbility;

    public string unitAbilityDes;

    public bool unitNextGen;

    public Order unitOrder;

    public Moves[] unitMoves;

    public Moves[] unitLearnableMoves;

    public BattleHUD unitHUD;

    public Moves unitActiveMove;


    public bool TakeDamage(float dmg)
    {
        unitCurHP -= dmg; 

        if(unitCurHP <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Heal(float amount)
    {
        unitCurHP += amount;
        if (unitCurHP> unitMaxHP)
        {
            unitCurHP = unitMaxHP;
        }
    }



}
