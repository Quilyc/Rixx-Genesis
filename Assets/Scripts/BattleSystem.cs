﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BattleState { START, CHOOSING, CLASH, PLAYERTURN, ENEMYTURN, WON, LOST, DRAW}

public enum Rix { FREAT, BATTLEBOT}

public class BattleSystem : MonoBehaviour
{

    public GameObject playerPrefab;
    public GameObject enemyPrefab;

    public Transform playerBattleStation;
    public Transform enemyBattleStation;

    public Rix rixtest;

    public BattleState state;

    public Unit playerUnit;
    public Unit enemyUnit;

    public ChatLog chat;
    public MoveConditions moveConLink;

    public BattleHUD playerHUD;
    public BattleHUD enemyHUD;


    //public GameObject battleSystem;
    public GameObject chatLog;

    public Button[] moveButtons;
    public Text[] moveButtonsText;

    int turnCount = 0;

    public bool isDead;



    // Start is called before the first frame update
    void Start()
    {
        state = BattleState.START;
        StartCoroutine(SetupBattle());
        moveConLink.SetUpMoves(playerUnit.unitMoves);
      
     
    }

    IEnumerator SetupBattle()
    {


        GameObject PlayerGo = Instantiate(playerPrefab, playerBattleStation);
        playerUnit = PlayerGo.GetComponent<Unit>();
        playerUnit.unitHUD = playerHUD;

        GameObject enemyGo = Instantiate(enemyPrefab, enemyBattleStation);
        enemyUnit = enemyGo.GetComponent<Unit>();
        enemyUnit.unitHUD = enemyHUD;

        chat.AddText("A wild " + enemyUnit.unitName + " approaches........");

        playerUnit.unitHUD.SetHUD(playerUnit);
        enemyUnit.unitHUD.SetHUD(enemyUnit);

        yield return new WaitForSeconds(2f);

        state = BattleState.CHOOSING;
        Choosing();

    }

    void Choosing()
    {
        turnCount += 1;
        chat.AddText("Turn " + turnCount);
        chat.AddText("Choose an action");

        int enemyMoveChance = Random.Range(0, 4);
        enemyUnit.unitActiveMove = enemyUnit.unitMoves[enemyMoveChance];

    }

    
    IEnumerator PlayerAttack()
    {
        bool isDead = enemyUnit.TakeDamage(playerUnit.unitAtk);

        enemyHUD.SetHP(enemyUnit.unitCurHP);

        chat.AddText("the attack is successful");

        yield return new WaitForSeconds(2f);

        if (isDead)
        {
            //End the Battle
            state = BattleState.WON;
            EndBattle();
             
        }
        else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn()); 
            //EnemyTurn
        }
        




        yield return new WaitForSeconds(2f);
    }
   

    IEnumerator EnemyTurn()
    {
        state = BattleState.ENEMYTURN;

        moveConLink.UseMove(enemyUnit.unitActiveMove, enemyUnit, playerUnit);

        yield return new WaitForSeconds(2f);

        if (playerUnit.unitOrder != Order.CLASH)
        {
            if(playerUnit.unitOrder == Order.FIRST)
            {
                DecideWinEnemy();
            }
                
                
        }
        

    }

    IEnumerator PlayerHeal()
    {
        playerUnit.Heal(5);

        playerHUD.SetHP(playerUnit.unitCurHP);
        chat.AddText(" You feel renewed strength");

        yield return new WaitForSeconds(2f);

        state = BattleState.ENEMYTURN;
        StartCoroutine(EnemyTurn());

    }


    void EndBattle()
    {
        if(state == BattleState.WON)
        {
            chat.AddText("You won the battle");
        }
        else if(state == BattleState.LOST)
        {
            chat.AddText("You were defeated");
        }
        else if(state == BattleState.DRAW)
        {
            chat.AddText("It is a draw");
        }



    }

    void DecideWinPlayer()
    {
        if (isDead)
        {
        
            state = BattleState.WON;
            EndBattle();

        }
        else
        {

            if(playerUnit.unitOrder == Order.FIRST)
            {
                state = BattleState.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if(playerUnit.unitOrder == Order.SECOND)
            {
                state = BattleState.CHOOSING;
                Choosing();
            }
          
        }
    }

    void DecideWinEnemy()
    {
        if (isDead)
        {
         
            state = BattleState.LOST;
            EndBattle();

        }
        else
        {
            if(playerUnit.unitOrder == Order.FIRST)
            {
                Choosing();
            }

           
            
        }
    }

    void DecideWinClash()
    {
        if(playerUnit.unitCurHP > 0 && enemyUnit.unitCurHP > 0)
        {
            state = BattleState.CHOOSING;
            Choosing();
        }
        else if (playerUnit.unitCurHP <= 0 && enemyUnit.unitCurHP <= 0)
        {
            state = BattleState.DRAW;
            EndBattle();

        }
        else if(playerUnit.unitCurHP > 0 && enemyUnit.unitCurHP <= 0)
        {
            state = BattleState.WON;
            EndBattle();
        }
        else if (playerUnit.unitCurHP <= 0 && enemyUnit.unitCurHP > 0)
        {
            state = BattleState.LOST;
            EndBattle();
        }


    }

    IEnumerator OrderConditions()
    {
        moveConLink.DecideOrder();
        if (playerUnit.unitOrder == Order.SECOND)
        {
            
            yield return StartCoroutine(EnemyTurn());
            
            yield return StartCoroutine(PlayerTurn());

            yield return StartCoroutine(Ending());

        }
        else if (playerUnit.unitOrder == Order.CLASH)
        {
            state = BattleState.CLASH;
            chat.AddText("A Clash is Happening!");
            int clashChance = Random.Range(1, 3);
            if (clashChance == 1)
            {
                state = BattleState.ENEMYTURN;
                StartCoroutine(EnemyTurn());
                state = BattleState.PLAYERTURN;
                moveConLink.UseMove(playerUnit.unitActiveMove, playerUnit, enemyUnit);
                DecideWinClash();
            }
        }
        else if (playerUnit.unitOrder == Order.FIRST)
        {
            state = BattleState.PLAYERTURN;
            moveConLink.UseMove(playerUnit.unitActiveMove, playerUnit, enemyUnit);
            DecideWinPlayer();
        }

        yield return new WaitForSeconds(0f);
    }

    public IEnumerator PlayerTurn()
    {

        state = BattleState.PLAYERTURN;
        moveConLink.UseMove(playerUnit.unitActiveMove, playerUnit, enemyUnit);
        yield return new WaitForSeconds(2f);
        if (playerUnit.unitOrder != Order.CLASH)
        {


            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        
        DecideWinPlayer();

      
    }





    public void OnAttackButton()
    {
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());

    }

    public void OnHealButton()
    {
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal());

    }


    public void OnMove1Button()
    {

        playerUnit.unitActiveMove = playerUnit.unitMoves[0];
        StartCoroutine(OrderConditions());

    }

    public void OnMove2Button()
    {

        playerUnit.unitActiveMove = playerUnit.unitMoves[1];
        StartCoroutine(OrderConditions());
    }

    public void OnMove3Button()
    {
        playerUnit.unitActiveMove = playerUnit.unitMoves[2];
        StartCoroutine(OrderConditions());
    }

    public void OnMove4Button()
    {
        playerUnit.unitActiveMove = playerUnit.unitMoves[3];
        StartCoroutine(OrderConditions());
    }

    public void OnSuperButton()
    {

    }

    public void OnItemButton()
    {

    }


    public void OnSwitchButton1()
    {

    }

    public void OnSwitchButton2()
    {

    }

    public void OnSwitchButton3()
    {

    }

    public void OnSwitchButton4()
    {

    }


    public void OnSwitchButton5()
    {

    }

    public void OnSwitchButton6()
    {

    }

    public void OnForwardButton()
    {

    }

    public void OnBackwardsButton()
    {

    }

    public IEnumerator Ending()
    {
        yield return new WaitForSeconds(2f);
    }

  










}
