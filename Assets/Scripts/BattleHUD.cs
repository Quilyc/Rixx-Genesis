﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleHUD : MonoBehaviour
{

    public Text nameText;
  
    public Slider hpSlider;

    public float chatLogInc;

    public float chatLogHeight;
    public float chatLogWidth;

    float count = 1;

  

    public void SetHUD(Unit unit)
    {
        nameText.text = unit.unitName;
        
        hpSlider.maxValue = unit.unitMaxHP;
        hpSlider.value = unit.unitCurHP;

    }

    public void SetHP(float hp)
    {
        hpSlider.value = hp; 
    }

}
