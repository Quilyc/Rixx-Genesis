﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveConditions : MonoBehaviour
{



    public BattleSystem battleLink;

    public StatusAndModifiers modLink;

    float fDamage;

    int count;


    public void DecideOrder()
    {

        if (battleLink.playerUnit.unitActiveMove.moveEffect != MoveEffects.ATTACKINGPRIORITY && battleLink.enemyUnit.unitActiveMove.moveEffect != MoveEffects.ATTACKINGPRIORITY)
        {
            if (battleLink.playerUnit.unitSpe > battleLink.enemyUnit.unitSpe)
            {
                battleLink.playerUnit.unitOrder = Order.FIRST;
                battleLink.enemyUnit.unitOrder = Order.SECOND;

            }
            else if (battleLink.enemyUnit.unitSpe > battleLink.playerUnit.unitSpe)
            {

                battleLink.playerUnit.unitOrder = Order.SECOND;
                battleLink.enemyUnit.unitOrder = Order.FIRST;
            }
            else
            {
                battleLink.playerUnit.unitOrder = Order.CLASH;
                battleLink.enemyUnit.unitOrder = Order.CLASH;
            }
        }

        else if (battleLink.playerUnit.unitActiveMove.moveEffect == MoveEffects.ATTACKINGPRIORITY && battleLink.enemyUnit.unitActiveMove.moveEffect != MoveEffects.ATTACKINGPRIORITY)
        {
            battleLink.playerUnit.unitOrder = Order.FIRST;
            battleLink.enemyUnit.unitOrder = Order.SECOND;
        }

        else if (battleLink.playerUnit.unitActiveMove.moveEffect != MoveEffects.ATTACKINGPRIORITY && battleLink.enemyUnit.unitActiveMove.moveEffect == MoveEffects.ATTACKINGPRIORITY)
        {
            battleLink.playerUnit.unitOrder = Order.SECOND;
            battleLink.enemyUnit.unitOrder = Order.FIRST;
        }

        else if (battleLink.playerUnit.unitActiveMove.moveEffect == MoveEffects.ATTACKINGPRIORITY && battleLink.enemyUnit.unitActiveMove.moveEffect == MoveEffects.ATTACKINGPRIORITY)
        {
            if (battleLink.playerUnit.unitSpe > battleLink.enemyUnit.unitSpe)
            {
                battleLink.playerUnit.unitOrder = Order.FIRST;
                battleLink.enemyUnit.unitOrder = Order.SECOND;

            }
            else if (battleLink.enemyUnit.unitSpe > battleLink.playerUnit.unitSpe)
            {

                battleLink.playerUnit.unitOrder = Order.SECOND;
                battleLink.enemyUnit.unitOrder = Order.FIRST;
            }
            else
            {
                battleLink.playerUnit.unitOrder = Order.CLASH;
                battleLink.enemyUnit.unitOrder = Order.CLASH;
            }

        }

    }







    private void Start()
    {

    }


    public void SetUpMoves(Moves[] tempMovesArray)
    {
        for (int i = 0; i < tempMovesArray.Length; i++)
        {

            ColorBlock colors = battleLink.moveButtons[i].colors;

            switch (tempMovesArray[i].moveType)
            {
                case Types.ICE:
                    colors.normalColor = Color.cyan;
                    battleLink.moveButtons[i].colors = colors;
                    break;
                case Types.FIRE:
                    colors.normalColor = Color.red;
                    battleLink.moveButtons[i].colors = colors;
                    break;
                case Types.WATER:
                    colors.normalColor = Color.blue;
                    battleLink.moveButtons[i].colors = colors;
                    break;

            }


            battleLink.moveButtonsText[i].text = battleLink.playerUnit.unitMoves[i].moveName;

        }
    }

    public void UseMove(Moves tempMove, Unit attacker, Unit defender)
    {
        switch (tempMove.moveEffect)
        {
            case MoveEffects.ATTACKING:
                StartCoroutine(AttackingMove(tempMove, attacker, defender));
                break;

            case MoveEffects.ATTACKINGMULTIPLE:
                StartCoroutine(AttackingMultipleMove(tempMove, attacker, defender));
                break;

            case MoveEffects.ATTACKINGPENETRATING:
                StartCoroutine(AttackingPenetratingMove(tempMove, attacker, defender));
                break;


        }



    }


    public IEnumerator AttackingMove(Moves tempMove, Unit attacker, Unit defender)
    {

        switch (tempMove.moveStat)
        {
            case Stats.SPA:
                DamageCalculation(tempMove, attacker.unitSpa, defender.unitSpd, defender.unitType);
                DamageEffect(tempMove, attacker, defender);
                break;

            case Stats.ATK:

                DamageCalculation(tempMove, attacker.unitAtk, defender.unitDef, defender.unitType);
                DamageEffect(tempMove, attacker, defender);
                break;


            case Stats.ATKSPA:

                DamageCalculation(tempMove, (attacker.unitSpa * .5f) + (attacker.unitAtk * .5f), defender.unitSpd, defender.unitType);
                DamageEffect(tempMove, attacker, defender);
                break;

        }


        yield return new WaitForSeconds(2f);
    }

    public IEnumerator AttackingMultipleMove(Moves tempMove, Unit attacker, Unit defender)
    {
        switch (tempMove.moveStat)
        {
            case Stats.SPA:
                DamageCalculation(tempMove, attacker.unitSpa, defender.unitSpd, defender.unitType);
                DamageEffect(tempMove, attacker, defender);
                break;

            case Stats.ATK:
                DamageCalculation(tempMove, attacker.unitAtk, defender.unitDef, defender.unitType);
                DamageEffect(tempMove, attacker, defender);
                break;
        }


        battleLink.chat.AddText("test");
        yield return new WaitForSeconds(1f);
        battleLink.chat.AddText("test");
        yield return new WaitForSeconds(1f);
        battleLink.chat.AddText("test");
        yield return new WaitForSeconds(1f);
        battleLink.chat.AddText("test");
        yield return new WaitForSeconds(1f);
        battleLink.chat.AddText("test");
        yield return new WaitForSeconds(1f);

        yield break;
        /*
        while(count < tempMove.moveHits - 1)
        {
            yield return new WaitForSeconds(1f);

            battleLink.isDead = defender.TakeDamage(fDamage);
            battleLink.enemyHUD.SetHP(defender.unitCurHP);
            battleLink.chat.AddText("It did " + fDamage + " damage");
            count += 1;

       }
       */

    }

    public IEnumerator AttackingPenetratingMove(Moves tempMove, Unit attacker, Unit defender)
    {
        switch (tempMove.moveStat)
        {
            case Stats.SPA:
                DamageCalculation(tempMove, attacker.unitSpa, defender.unitSpd * tempMove.movePen, defender.unitType);
                DamageEffect(tempMove, attacker, defender);
                break;

            case Stats.ATK:
                DamageCalculation(tempMove, attacker.unitAtk, defender.unitDef * tempMove.movePen, defender.unitType);
                DamageEffect(tempMove, attacker, defender);
                break;
        }

        yield return new WaitForSeconds(2f);

    }

    public IEnumerator AttackingModifierMove(Moves tempMove)
    {

        yield break;

    }

    public IEnumerator BoostingMove()
    {
        yield break;
    }

    public IEnumerator AttackingRecoilMove()
    {
        yield return new WaitForSeconds(2f);
    }

    public IEnumerator AttackingDecreasingMove()
    {
        yield return new WaitForSeconds(2f);
    }

    public IEnumerator AttackingPriorityMove()
    {


        yield return new WaitForSeconds(2f);
    }

    public void DamageCalculation(Moves tempMove, float attackingStat, float defendingStat, Types defenderType)
    {
        fDamage = tempMove.moveDmg * (attackingStat / defendingStat) * modLink.MoveTypingModifier(tempMove.moveType, defenderType);


    }

    public void DamageEffect(Moves tempMove, Unit attacker, Unit defender)
    {
        battleLink.isDead = defender.TakeDamage(fDamage);
        defender.unitHUD.SetHP(defender.unitCurHP);
        battleLink.chat.AddText(attacker.unitName + " Used " + tempMove.moveName);

        if (modLink.modifier != 0f)
        {
            battleLink.chat.AddText("It did " + fDamage + " damage");

            if (modLink.modifier == 4f)
            {
                battleLink.chat.AddText("It was SUPER effective");
            }

            if (modLink.modifier == 2f)
            {
                battleLink.chat.AddText("It was effective!");
            }

            else if (modLink.modifier == 1f)
            {
                battleLink.chat.AddText("A neutral hit");
            }

            else if(modLink.modifier < 1f && modLink.modifier> 0f)
            {
                battleLink.chat.AddText("It was resisted");
            }

        }
 
        else if (modLink.modifier == 0f)
        {
            battleLink.chat.AddText("It had no effect");
        }


    }

}

   